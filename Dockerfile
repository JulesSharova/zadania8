FROM alpine:3.7

RUN apk add python3

COPY src /app
WORKDIR /app
RUN pip3 install -r requirements.txt

CMD ["python3", "app.py"]

EXPOSE 8080